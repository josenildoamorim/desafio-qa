#language: pt-br

Funcionalidade: Enviar anexos

Cenário: Enviar um contato
	Dado que estou em uma conversa
	Quando eu seleciono o botão Anexar
		E seleciono o botão Contato
		E seleciono o contato “Josenildo”
	Então a aplicação retorna para a conversa
		E exibe o contato enviado

Cenário: Enviar um documento 
	Dado que estou em uma conversa
	Quando eu seleciono o botão Anexar
		E seleciono o botão Documento
		E seleciono o documento “teste.txt”
		E seleciono Enviar
	Então a aplicação a retorna para a conversa
		E exibe o documento enviado

Cenário: Enviar uma localização
	Dado que estou em uma conversa
	Quando eu seleciono o botão Anexar
		E seleciono o botão Local
		E seleciono o botão Enviar sua localização atual
	Então a aplicação a retorna para a conversa
		E exibe a localização atual compartilhada


Funcionalidade: Realizar uma chamada telefônica

Cenário: Realizar chamada através de uma conversa
	Dado que estou na aba Conversas
	Quanto eu abro uma conversa
		E seleciono o botão Chamar
	Então a aplicação inicia uma nova chamada

Cenário: Realizar chamada através da aba Chamadas
	Dado que estou na aba Chamadas
	Quanto eu seleciono o botão Nova Ligação
		E seleciono um contato
	Então a aplicação inicia uma nova chamada

Cenário: Realizar chamada através de uma chamada anterior
	Dado que estou na aba Conversas
	Quanto eu seleciono a aba Chamadas
		E seleciono um contato recente
		E seleciono o botão Chamar
	Então a aplicação inicia uma nova chamada
