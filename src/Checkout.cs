﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.Tests
{
    public class Checkout
    {
        private readonly List<Item> items = new List<Item>();
        private readonly List<RegraPreco> regrasPreco = new List<RegraPreco>();

        public int CalculaTotal()
        {
            int total = 0;
            var grupoItens = items.GroupBy(g => g.Nome);

            foreach (var itemGroup in grupoItens)
            {
                total += PrecoTotalParaCadaGrupo(itemGroup);
            }
            return total;
        }

        private int PrecoTotalParaCadaGrupo(IGrouping<object, Item> itemGroup)
        {
            int total = 0;
            var regraParaGrupo = regrasPreco.FirstOrDefault(r => r.NomeItem == itemGroup.Key);
            if (regraParaGrupo != null)
            {
                var extra = itemGroup.Count() - regraParaGrupo.ItemCount;
                if (extra < 0)
                {
                    total += itemGroup.Sum(g => g.Preco);
                }
                else
                {
                    total += regraParaGrupo.Total;
                    total += extra * itemGroup.First().Preco;
                }
            }
            else
            {
                total += itemGroup.Sum(x => x.Preco);
            }

            return total;
        }

        public void AdicionaItem(Item item)
        {
            items.Add(item);
        }

        public void AdicionaRegraPreco(RegraPreco rule)
        {
            regrasPreco.Add(rule);
        }

        public class Item
        {
            public object Nome { get; set; }
            public int Preco { get; internal set; }
        }

        public class RegraPreco
        {
            public int ItemCount { get; internal set; }
            public string NomeItem { get; internal set; }
            public int Total { get; internal set; }
        }
    }
}
    
    