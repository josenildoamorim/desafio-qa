﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Checkout.Tests.Checkout;

namespace Checkout.Tests
{
    [TestClass]
    public class CheckoutTests
    {
        private Checkout checkout;        

        private readonly Item itemA = new Item
        {
            Nome = "A",
            Preco = 50
        };

        private readonly Item itemB = new Item
        {
            Nome = "B",
            Preco = 30
        };

        private readonly RegraPreco RegraItemA = new RegraPreco
        {
            NomeItem = "A",
            ItemCount = 3,
            Total = 130
        };

        private readonly RegraPreco RegraItemB = new RegraPreco
        {
            NomeItem = "B",
            ItemCount = 2,
            Total = 45
        };


        [TestInitialize]
        public void SetUp()
        {
            checkout = new Checkout();
        }

        private void AdicionaItem(int count,Item item)
        {
            for (int i = 0;
                 i < count;
                 i++)
            {
                checkout.AdicionaItem(item);
            }
        }

        [TestMethod]
        public void CheckoutDeUmItem()
        {
            AdicionaItem(1,itemA);
            int result = checkout.CalculaTotal();
            Assert.AreEqual(50,result);
        }

        [TestMethod]
        public void CheckoutComDescontoRetornaDesconto()
        {
            checkout.AdicionaRegraPreco(RegraItemA);
            AdicionaItem(3,itemA);
            int result = checkout.CalculaTotal();
            Assert.AreEqual(130,result);
        }

        [TestMethod]
        public void TestCheckoutComPrecoDescontoEItemExtra()
        {
            checkout.AdicionaRegraPreco(RegraItemA);
            AdicionaItem(4,itemA);
            int result = checkout.CalculaTotal();
            Assert.AreEqual(180,result);
        }

        [TestMethod]
        public void CheckoutComMultiplosGruposDeItensComDesconto()
        {
            checkout.AdicionaRegraPreco(RegraItemA);
            checkout.AdicionaRegraPreco(RegraItemB);
            AdicionaItem(4,itemA);
            AdicionaItem(3,itemB);
            int result = checkout.CalculaTotal();
            Assert.AreEqual(255,result);
        }
    }
}
